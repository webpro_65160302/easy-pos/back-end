export class CreateUserDto {
  email: string;
  fullName: string;
  password: string;
  gender: string;
  roles: string;
  image: string;
}
